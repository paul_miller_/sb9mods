
# Additional bash aliases 

alias celery='celery -A insight.celeryproj'
alias ls='ls --color=auto'

QA_DIR=/home/ampush/services/qa/tools/test

alias preserve_mpa_logs='$QA_DIR/preserve_mpa_logs.sh'
alias preserve_amp_logs='$QA_DIR/preserve_amp_logs.sh'

alias start_amp_services='$QA_DIR/start_amp_services.sh'
alias start_mpa_services='sudo su ampush $QA_DIR/start_mpa_services.sh'

alias stop_amp_services='$QA_DIR/stop_amp_services.sh'
alias stop_mpa_services='sudo su ampush $QA_DIR/stop_mpa_services.sh'

alias wipe_amp_logs='$QA_DIR/wipe_amp_logs.sh'
alias wipe_mpa_logs='$QA_DIR/wipe_mpa_logs.sh'

alias stop_services='$QA_DIR/stop_amp_services.sh && sudo su ampush $QA_DIR/stop_mpa_services.sh'
alias clear_logs='$QA_DIR/wipe_amp_logs.sh; $QA_DIR/wipe_mpa_logs.sh'
alias start_services='$QA_DIR/start_amp_services.sh && /etc/init.d/httpd stop'
