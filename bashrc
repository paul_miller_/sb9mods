# .bashrc

PATH="$PATH"

parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

export PYTHONPATH=/var/www/www.ampushinsight.com/insight/source:/home/ampush/services/aps:/home/ampush/services/ot:/home/ampush/services:/home/ampush/services/qa
export DJANGO_SETTINGS_MODULE=insight.settings

# User specific aliases and functions
. $HOME/bin/alias.sh

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

alias ls='ls'
alias ll='ls -al'
export PS1='$(hostname | cut -d'.' -f1):$PWD> '
export WORKON_HOME=~/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

. /home/ampush/init_sshagent.sh

alias papply='puppet apply --debug /etc/puppet/manifests/site.pp'

source ~/.bash_aliases
