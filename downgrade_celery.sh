#!/bin/bash

pip uninstall celery
pip uninstall kombu
pip uninstall billiard
pip install git+ssh://git@bitbucket.org/ampushsocial/python-billiard.git@celery-3.1rc4-sept5#egg=billiard
pip install git+ssh://git@bitbucket.org/ampushsocial/python-kombu.git@celery-3.1rc4-sept5#egg=kombu
pip install git+ssh://git@bitbucket.org/ampushsocial/python-celery.git@celery-3.1rc4-sept5-a3#egg=celery
pip install amqp==1.2.1
pip install amqplib==1.0.2