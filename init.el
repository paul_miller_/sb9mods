;; Save backup and autosaves to ~/.emacs.d/.tmp
(setq
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
    '((".*" . "~/.emacs.d/.tmp"))    ; don't litter my fs tree
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t) 

;; Set up the package manager.
;; Use elpa, melpa, and marmalade
(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
  (add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
  )

;; Set the path for the emacs process so things like running .bashrc
;; when opening a terminal work
(setenv "PATH" (shell-command-to-string "source ~/.bash_profile; echo -n $PATH"))

;; Show the menu bar when we have a window system, otherwise not.

(menu-bar-mode (window-system))

;; Line numbers on the left
(global-linum-mode t)

;; Show column number in status bar
(column-number-mode)

;; Put a little space after the line numbers so as not to crowd the
;; text.
(setq linum-format "%d  ")

;; Set the mode to python-mode when the filename ends in ".py"
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))

;; Don't highlight the current line.  It's annoying!

(global-hl-line-mode 0)
