import redis
import socket
from json import loads, dumps

redis_connection = redis.Redis(host='sb11-plat1')


HOSTNAME = socket.gethostname()


INSIGHT_CONFIG = {
    'servers': {
        'mongo': {
            'HOST': 'db1-plat1.ampush1.com',
            'PORT': '27017',
            'DATABASES': [
                {
                    'NAME': 'facebook',
                    'USER': None,
                    'PASSWORD': None
                },
                {
                    'NAME': 'targeting',
                    'USER': None,
                    'PASSWORD': None
                },
                {
                    'NAME': 'job',
                    'USER': None,
                    'PASSWORD': None
                }
            ]
        },
        'database': {
            'master': {
                'ENGINE': 'mysql',
                'NAME': 'celery_test_ampush_db1',
                'HOST': 'db1-plat1.ampush1.com',
                'PORT': '3306',
                'USER': 'ampush_db1',
                'PASSWORD': 'ampush_db1',
                'OPTIONS': {
                    'init_command': 'SET storage_engine=INNODB',
                }
            },
            'slaves': [{
                    'ENGINE': 'mysql',
                    'NAME': 'celery_test_ampush_db1',
                    'HOST': 'db1-plat1.ampush1.com',
                    'PORT': '3306',
                    'USER': 'ampush_db1',
                    'PASSWORD': 'ampush_db1',
                    'OPTIONS': {
                        'init_command': 'SET storage_engine=INNODB',
                    }
                },
            ],
        },
        'redis': {
            'master': {
                'ip': 'sb11-plat1.ampush1.com',
                'port': 6379,
            },
            'slaves': [],
        },
        'redis-celery-broker': {
            'master': {
                'ip': 'sb10-plat1.ampush1.com',
                'port': 6379,
            },
        },
        'redis-celery-pubsub': {
            'master': {
                'ip': 'sb11-plat1.ampush1.com',
                'port': 6379,
            },
        },
        'redis-celery-results': {
            'master': {
                'ip': 'sb12-plat1.ampush1.com',
                'port': 6379,
            },
        },
        'memcache': [{
                'ip': 'sb11-plat1.ampush1.com',
                'port': '11211',
            },
        ],
        'syslog': {
                'ip'       : '/dev/log',
                'port'     : 0,
                'facility' : 16,
        },
    'aps' : {
        'RGeventTransport': {
                                  'ip': '127.0.0.1',
                                  'port': 8080,
                                  'pool_size': 25
        },
        'RESTTransport' : {
                          'ip': '127.0.0.1',
                          'port': 4040,
                          'pool_size': 25
                 }
        },


	# ot/apiserver
	# TODO: for consistancy should point to 127.0.0.1 
	# instead of eth0 if possible
        'ots': [{
                'ip': 'sb11-plat1.ampush1.com',
                'port': 5050,
            }
        ],

        'fbproxy': [{
                'ip': 'sb11-plat1.ampush1.com',
                'port': 9090,
        'response_port_forwarded' : -1
            }
        ],
  },
}

APS_CONFIG = {
    'APSW_PROCESSES': 24,
    'CASSANDRA_HOSTS': ['db1-plat1.ampush1.com'],
    'CASSANDRA_KEYSPACE': 'aps',
    'CASSANDRA_PORT': 9042,
    'GELF_HOST': 'cmanager.ampush1.com',
    'GELF_PORT': 12201,

 # APSR Production Logging Handlers
    'APSR_PRODUCTION_LOGGING_CONFIGURATION': {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s | %(levelname)s | %(filename)s | Line: %(lineno)d | %(message)s'
            },
        },
        'handlers': {
            'aps_file_handler': {
                'class': 'logging.handlers.WatchedFileHandler',
                'formatter': 'standard',
                'filename': '/home/ampush/insightlogs/aps/apsr.log',
            }
        },
        'loggers': {
            'aps': {
                'handlers': ['aps_file_handler'],
                'level': 'INFO',
                'propagate': 0
            },
        }
    },

    # APSW Production Logging Handlers
    'APSW_PRODUCTION_LOGGING_CONFIGURATION': {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s | %(levelname)s | %(filename)s | Line: %(lineno)d | %(message)s'
            },
        },
        'handlers': {
            'aps_file_handler': {
                'class': 'logging.handlers.WatchedFileHandler',
                'formatter': 'standard',
                'filename': '/home/ampush/insightlogs/aps/apsw.log',
            }
        },
        'loggers': {
            'aps': {
                'handlers': ['aps_file_handler'],
                'level': 'INFO',
                'propagate': 0
            },
        }
    },


    'LOG_DIRECTORY': '/home/ampush/insightlogs/aps/',

    'METRIC_OBJECT_ID_SAVE': True,

    'PUBSUB_CHANNELS': {
       HOSTNAME.split('.', 1)[0]: ['aps_insight.facebook.*.graph_account_adgroups_stats',
          'aps_insight.facebook.*.graph_get_account_stats',
          'aps_insight.facebook.*.graph_get_account_conversion_stats',
          'aps_insight.facebook.*.graph_get_all_ad_conversion_stats_for_acct',
          'aps_insight.facebook.populator.*.tracker_mapping',
          'aps_insight.calfinder.*.*get_conversions',
          'aps_insight.ots.hasoffers.*.get_conversions',
          'aps_insight.ots.hasoffers.*.get_account_conversions']
    },

    'REDIS_CONFIG': {'redis_database': 0,
                     'redis_host': 'sb11-plat1.ampush1.com',
                     'redis_port': 6379}
}

INSIGHT_SITE_CONFIG = {
    'DEFAULT':{
        'DEBUG'              : False,
        'API_SIM'            : False,
        'DBENGINE'           : 'mysql',
        'MEDIA_ROOT'         : '/var/www/www.ampushinsight.com/insight/media/',
        'MEDIA_URL'          : 'https://sb11-plat1.ampush1.com/media/',
        'EXPORTER_WRKING_DIR': '/tmp/',
        'EXPORTER_ROOT'      : '/var/www/www.ampushinsight.com/insight/media/exports/',
        'STATIC_ROOT'        : '',
        'STATIC_URL'         : '/media/',
        'SITE_URL'           : 'https://sb11-plat1.ampush1.com/',
        'ADMIN_MEDIA_PREFIX' : 'https://sb11-plat1.ampush1.com/media/admin/',
        'TEMPLATE_DIR'       : '/var/www/www.ampushinsight.com/insight/source/insight',
        'STATIC_SERVE'       : False,
        'POPULATOR_THREADS'  : 32,
        'POPULATOR_LOG'      : '/home/ampush/insightlogs/populator/',
        'MARATHON_THREADS'   : 32,
        'MARATHON_LOG'       : '/home/ampush/insightlogs/marathon/',
        'LOG_DIRECTORY'      : '/home/ampush/insightlogs/',
        'INFO_EMAIL'         : 'info@ampush.com',
        'INFO_PASS'          : 'ampush123',
        'FB_MASTER_EMAIL'    : 'atish.davda@ampushsocial.com',
        'FB_MASTER_PASS'     : 'Facebook123',
        'EMAIL_HOST'         : 'cmanager.ampush1.com',
        'EMAIL_PORT'         : 25,
        'IPR_LOGDIR'         : '/home/ampush/insightlogs/ipr/',
        'IPR_LOGGER'         : 'IPR_LOGGER',
        'ACCESS_TOKEN'       : {
            '0':'CAACcXbmWPQMBALJvY3IfcdyTW8kCTdZAL9zlvWXF7C9l53UHyhAR4dcfiEUtSson3sARfYzaXQZCZADaFdwBCC7X8vEXd0DhabUuTEMCGJbjkDZBaja4WfCxKT26ZBFgv0E9Sngx353EtFvgaLZBM1Hn805Y10I1ZCbpQlrtgKleaZB0hiv3umQ46YXp9ylyn28ZD'
        },
        'SYSTEM_USER_TOKEN': {
            '0':'CAACcXbmWPQMBAJXhkZAMjoYddmCpmmZBZBGFL80N0xhEoYRBXHhE4iB19zbzVmDJjUKl7jlrSrOMQyqRZBOGW5NE36v82mn1YwEW0ZCxCGBZCbZBMEnqlXuqtjKEXSvd8R3jLrO5KjUS8VBc3cM90R6jyQ5Xg4Jd6dyzfMIWueWlrFwbaPCmB8edaAZAP0kXuFsZD'
        },
         'BUSINESS_MANAGER_ID': '308858415957152',
         'BUSINESS_MANAGER_PAGE_ID': '452786855563',
	'BUSINESS_MANAGER_ADMIN_USERS': ['100005898497244','100004749467369','100004163535885'],
	"CONVERSION_DB" : "calfinder",
	"CONVERSION_HOST" : "db1-plat1.ampush1.com",
	"CONVERSION_PASSWORD" : "ampush_db1",
	"CONVERSION_PORT" : 3306,
	"CONVERSION_USER" : "ampush_db1",
        'FACEBOOK_APP_ID'    : '503229176405924',
        'FACEBOOK_APP_SECRET': 'cb22b3d2c8f3232a9baeacf47a68e737',
        'FACEBOOK_SCOPE'     : 'manage_pages,publish_actions,publish_stream,publish_checkins,manage_notifications,ads_management,read_requests,read_mailbox,read_insights,user_work_history,user_website,user_videos,user_status,user_religion_politics,user_relationship_details,user_relationships,user_location,user_about_me',
        'CREDENTIAL_PRIV_KEY': 'MIICWQIBAAKBgQDwFIowZtgNP8Jde+uTaHaR4ztdr4u7dh7K5raLRoLF7y4xGkZ/J198UfqHfcPCNFTLAl8n1wonaG52Lm/H3yvAKgn4MK+m1QziehiwPFJnkEqm+8NtoEmiW9ZXPKEvvfyTV3ZpgTBiQy/k0svFREYVxg/SwvSBlreFFLvfeQfstwIDAQABAn8NIRJtkzI9hz8Z3a0EYrIZSFP2y6LYnZ0Re4yAEVsVd/8OsGEqkvylz9Xjkv0hobsBXx/VDe2AOjXQQLncNFgwDv5rfbb4cEgdyfBdPbgwik6tcyy5LhhiVqpxhrZSxLjstPSdppUiDrBiLlE6fW8ZFqPj1sJRBAjtBZdLif/RAkEA9OULT7UafQYs9JtmPdI43f3nSPBx5uecCgWlS8jZsCjmPejbVst2s6FnIWvOJjP3PTlEnB1cc4qX2AHxWfeoUwJBAPr3mt6EZPf8j6kMxrPTvDt/n5D0YvOOkKxUrm3/qLGQJUBQzDkNciUYtu2z6FsZp0R7hWQdcmXk2eoh2XUcDY0CQCq8sf2mAEpjQoMpf204e54aCjvLYkVGlA+XrqIMGhlI+e1B7s010rBEcYf+lUpLdVEk5llMMm8jd55FsIXf12sCQEQXK9CVkSNrRbry+XJOzzJBZRSaCcLU+lGXRCAaDjXmywSRJ2ePS1nfQ7poZnFZG3XMIZvRFXmpxqnsdyh77P0CQDRtqXHj8O0cQhzQ5iePrG6Ex+zC16jDp5dJ5e94NByUmBLC2e4QZVTTaqRYR4ZUF2YWEQOxbAhEGnKXsq8krIY=',                  'CREDENTIAL_PUB_KEY' : 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDwFIowZtgNP8Jde+uTaHaR4ztdr4u7dh7K5raLRoLF7y4xGkZ/J198UfqHfcPCNFTLAl8n1wonaG52Lm/H3yvAKgn4MK+m1QziehiwPFJnkEqm+8NtoEmiW9ZXPKEvvfyTV3ZpgTBiQy/k0svFREYVxg/SwvSBlreFFLvfeQfstwIDAQAB',
        'ADMIN_LIST'         : (('Unfed Monkeys', 'qa03@ampush.com'),),
        'REPORT_LIST'        : ['qa_team@ampush.com'],
    },
}

OT_CONFIG = {
        'cassandra' : {
                    'cluster': ['db1-plat1.ampush1.com'],
                    'port': 9160,
                    'keyspace': 'ot'
                },

        'OT_LOG_DIRECTORY': '/home/ampush/insightlogs/ot/',
	'OT_DIRECTORY': '/home/ampush/services/ot/',
        'OT_COMMON_LOGFILE': 'ot_common.log',
        'OT_PIXEL_LOGFILE': 'pixel.log',
        'EMAIL_HOST': 'cmanager.ampush1.com',
        'EMAIL_PORT': 25,
        'EMAIL_FROM': 'qa03-donotreplay@ampush.com',
        'EMAIL_TO': 'qa_team@ampush.com',
        'GELF_HOST': 'cmanager.ampush1.com',
        'GELF_PORT': 12201
}

# Tokens Audit Trail
TOKENAUDIT_CONFIG = {
    'TOKENAUDIT_LOG_DIRECTORY': '/home/ampush/insightlogs/tokenaudit/',
    'LOG_FILENAME': 'tokenaudit.log',
    'EMAIL_HOST': 'cmanager.ampush1.com',
    'EMAIL_PORT': 25,
    'EMAIL_FROM': 'tokens@ampush.com',
    'EMAIL_TO': 'qa_team@ampush.com',
    'GELF_HOST': 'cmanager.ampush1.com',
    'GELF_PORT': 12201,
    'PROXY_SERVERS': 3,
    'TOKEN_URL': "https://sb11-plat1.ampush1.com/facebook/token",
    'PROXY_URL': "https://sb11-plat1.ampush1.com:9090/hb?cp=3",
    'EMAIL_PRIV': 'leo.celis@ampush.com',
    'EMAIL_PRIV_PASS': ''
}

# Provisioner
PROVISIONER_CONFIG = {
    'HOST': 'localhost',
    'PORT': 7575,
    'PROVISIONER_LOG_DIRECTORY': '/home/ampush/insightlogs/provisioner/',
    'LOG_FILENAME': 'provisioner.log',
    'EMAIL_HOST': 'cmanager.ampush1.com',
    'EMAIL_PORT': 25,
    'EMAIL_FROM': 'provisioner-fs1@ampush.com',
    'EMAIL_TO': 'qa@ampush.com',
    'GELF_HOST': 'cmanager.ampush1.com',
    'GELF_PORT': 12201
}

# Connects to insight for testing
QA_CONFIG = {
    'HOST': 'sb11-plat1.ampush1.com',
    'PORT': '80',
    'USER': 'qa',
    'PASSWORD': 'br34km3n0w',
    'AMP_ROLES': {
        'active': {'USER': 'qa_active', 'PASSWORD': 'br34km3n0w'}, 
        'staff': {'USER': 'qa_staff', 'PASSWORD': 'br34km3n0w'},
        'super': {'USER': 'qa_super', 'PASSWORD': 'br34km3n0w'}},
    'TEST_CONTRACTS': ['Ampush DEV', 'Ampush QA'],
    'TEST_CONTRACT_IDS': [567, 568],
    'TEST_ACCOUNTS': ['amtestpush_01', 'amtestpush_02', 'amtestpush_03', 'amtestpush_04', 'amtestpush_05', 'amtestpush_06', 'amtestpush_07', 'amtestpush_08', 'amtestpush_09', 'amtestpush_10', 'amtestpush_11', 'amtestpush_12', 'amtestpush_13', 'amtestpush_14', 'amtestpush_15', 'amtestpush_16', 'amtestpush_17', 'amtestpush_18', 'amtestpush_19', 'amtestpush_20', 'carnival1', 'carnival2'],
    'STRATEGIC_CONTRACTS': ['GREE - Knights & Dragons iPhone', 'HotelTonight Mobile App Installs', 'MasterCard Priceless Cities Q1 2014', 'Uber - Driver Acquisition', 'Zynga - Ninja Kingdom User Acquisition - Q4 2013'], 
    'STRATEGIC_ACCOUNTS': ['GREE 1', 'HotelTonight_2_US', 'HotelTonight_3_GB-BE-IE-NL', 'HotelTonight_4_MX-CA', 'HotelTonight_5_ES-FR', 'HotelTonight_6_DE-CH-IT', 'HotelTonight_7_US-FR', 'HotelTonight_8_US', 'Uber_20', 'Uber_19', 'Uber_21', 'uber_03', 'Uber_07', 'Uber_08', 'Uber_09', 'Uber_10', 'Uber_11', 'Uber_14', 'Uber_15', 'Uber_16', 'Uber_17', 'Uber_18', 'Uber_32', 'Uber_06', 'Uber_22', 'Uber_23', 'Uber_24', 'Uber_25', 'Uber_26', 'Uber_27', 'Uber_28', 'Uber_30', 'uber_31', 'uber_33', 'uber_34', 'uber_35', 'mastercard_39', 'mastercard5', 'mastercard6', 'mastercard_40', 'mastercard_26', 'mastercard_42', 'mastercard_29', 'mastercard_28', 'mastercard_27', 'mastercard_30', 'mastercard_31', 'mastercard_32', 'mastercard_33', 'mastercard_43', 'mastercard_44', 'mastercard_45', 'Zynga_03_US'],
    'CUSTOM_VIEW_CONTRACTS': ['Zynga - Ninja Kingdom User Acquisition - Q4 2013', 'Zynga - Ayakashi Mobile App Installs - Q4 2013', 'Zynga - CastleVille Mobile App Installs - Q4 2013', 'Electric Run CPL & CPA test campaign', 'Uber - Indianapolis Blanket Market Share Test - August 2013', 'LOFT DR Q4 2013', 'Ann Taylor DR Q4 2013'],
    'PAGEPOST_CONTRACTS': ['Uber - Indianapolis Blanket Market Share Test - August 2013', 'Ann Taylor DR Q4 2013'],
    'CAM_CONTRACTS': [''],
    'AUDIENCE_GROUP_CONTRACTS': ['Ann Taylor DR Q4 2013']
}


redis_connection.set('INSIGHT_CONFIG', dumps(INSIGHT_CONFIG))
redis_connection.set('INSIGHT_SITE_CONFIG', dumps(INSIGHT_SITE_CONFIG))
redis_connection.set('APS_CONFIG', dumps(APS_CONFIG))
redis_connection.set('OT_CONFIG', dumps(OT_CONFIG))
redis_connection.set('TOKENAUDIT_CONFIG', dumps(TOKENAUDIT_CONFIG))
redis_connection.set('PROVISIONER_CONFIG', dumps(PROVISIONER_CONFIG))
redis_connection.set('QA_CONFIG', dumps(QA_CONFIG))

redis_connection.persist('INSIGHT_CONFIG')
redis_connection.persist('INSIGHT_SITE_CONFIG')
redis_connection.persist('APS_CONFIG')
redis_connection.persist('OT_CONFIG')
redis_connection.persist('TOKENAUDIT_CONFIG')
redis_connection.persist('PROVISIONER_CONFIG')
redis_connection.persist('QA_CONFIG')
