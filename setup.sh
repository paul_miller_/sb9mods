#!/bin/bash

source bash_aliases

echo Stopping MPA services...
sudo su ampush /home/ampush/services/qa/tools/test/stop_mpa_services.sh

echo Stopping AMP services...
/home/ampush/services/qa/tools/test/stop_amp_services.sh

echo Copying celery config files...

CELERY_CONFIG_DIR=/etc/default/`hostname -s`
mkdir -p $CELERY_CONFIG_DIR
cp -f celery/* $CELERY_CONFIG_DIR/
cp -f celeryd /etc/init.d/celeryd

echo Checking if user paul exists...
if getent passwd paul > /dev/null 2>&1; then
    echo User paul already exists.
else
    echo Creating user paul...
    useradd -G wheel paul
    passwd paul
fi

echo Customizing bash config files...
yum -y install figlet

cp -f bash_profile_additions ~root/.bash_profile_additions
cp -f bash_aliases ~root/.bash_aliases
cp -f bash_profile ~root/.bash_profile
cp -f bashrc ~root/.bashrc
cp -f rmcolors ~root/bin/rmcolors

echo Copying emacs config...
mkdir -p ~/.emacs.d/.tmp
cp -f init.el ~/.emacs.d

echo Copying zabbix config...
rm -rf /etc/zabbix
cp -rf zabbix /etc/zabbix
/etc/init.d/zabbix-agent restart

echo Setting up git config...
git config --global user.name "Paul Miller"
git config --global user.email paul.miller@ampush.com
git config --global core.editor emacs
